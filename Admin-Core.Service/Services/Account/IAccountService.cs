﻿using Admin_Core.Models.ApiModels.AccountModels;
using Admin_Core.Models.DataModels;

namespace Admin_Core.Service.Services.Account
{
    public interface IAccountService
    {
        ProfileApiModel Register(RegisterApiModel model);
        ProfileApiModel Login(string email, string password);
        ProfileApiModel GetProfile(int userId);
        ProfileApiModel UpdateProfile(int userId, UpdateProfileApiModel model);
        string GenerateToken(ProfileApiModel model, int expiredHours);
    }
}
