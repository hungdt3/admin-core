﻿using Admin_Core.Common.Attributes;
using Admin_Core.Common.Enums;
using Admin_Core.Common.Exceptions;
using Admin_Core.Common.Helpers;
using Admin_Core.DataAccess.Repositories;
using Admin_Core.Models.ApiModels.AccountModels;
using Admin_Core.Models.DataModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Admin_Core.Service.Services.Account.Implements
{
    [Service(typeof(IAccountService), ServiceLifetime.Scoped)]
    public class AccountService : IAccountService
    {
        private readonly AppSettings _appSetting;
        private readonly IUserRepository _userRepository;

        public AccountService(
             IOptions<AppSettings> optionAppSettings,
             IUserRepository userRepository
            )
        {
            _appSetting = optionAppSettings.Value;
            _userRepository = userRepository;
        }

        public ProfileApiModel Register(RegisterApiModel model)
        {
            var existedItem = _userRepository.GetByEmail(model.Email);
            if (existedItem != null)
            {
                throw new CustomException(ApiResultCode.ItemAlreadyExist);
            }

            var user = new User()
            {
                Name = model.Name,
                Email = model.Email,
                MobileNumber = model.MobileNumber,
                Password = EncryptionHelper.EncryptSHA1(model.Password),
                Gender = model.Gender,
                DoB = model.DoB,
                EmailOptIn = model.EmailOptIn
            };

            _userRepository.Insert(user);

            return new ProfileApiModel(user);
        }

        public ProfileApiModel Login(string email, string password)
        {
            var user = _userRepository.GetByEmail(email);
            if (user == null)
            {
                return null;
            }

            if (!string.Equals(EncryptionHelper.EncryptSHA1(password), user.Password))
            {
                return null;
            }

            return new ProfileApiModel(user);
        }

        public ProfileApiModel GetProfile(int userId)
        {
            var user = _userRepository.GetByID(userId);
            if (user == null)
            {
                throw new CustomException(ApiResultCode.ItemDoesNotExist);
            }

            return new ProfileApiModel(user);
        }

        public ProfileApiModel UpdateProfile(int userId, UpdateProfileApiModel model)
        {
            var existedItem = _userRepository.GetByID(userId);
            if (existedItem == null)
            {
                throw new CustomException(ApiResultCode.ItemDoesNotExist);
            }

            existedItem.Name = model.Name;
            existedItem.MobileNumber = model.MobileNumber;
            existedItem.Gender = model.Gender;
            existedItem.DoB = model.DoB;
            existedItem.EmailOptIn = model.EmailOptIn;

            _userRepository.Update(existedItem);

            return new ProfileApiModel(existedItem);
        }

        public string GenerateToken(ProfileApiModel model, int expiredHours)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Sid, model.ID.ToString()),
                new Claim(ClaimTypes.Email, model.Email),
                // Add role here
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSetting.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "me",
                audience: "you",
                claims: claims,
                expires: DateTime.Now.AddHours(expiredHours),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
