﻿using Admin_Core.Common.Enums;
using Admin_Core.Common.Exceptions;
using Admin_Core.DataAccess.Base;
using Admin_Core.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Admin_Core.Service.Base
{
    public class Manager<T> : IManager<T>
        where T : BaseTable
    {
        protected readonly IRepository<T> _repository;

        public Manager(IRepository<T> repository)
        {
            _repository = repository;
        }

        public virtual IEnumerable<T> GetList()
        {
            return _repository.GetAll().AsEnumerable();
        }

        public virtual T GetItem(int id)
        {
            return _repository.GetByID(id);
        }

        public IEnumerable<T> GetItems(IEnumerable<int> ids)
        {
            return _repository.Find(item => ids.Contains(item.ID));
        }

        public virtual T AddItem(T item)
        {
            try
            {
                _repository.Insert(item);
                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual IEnumerable<T> AddItems(IEnumerable<T> items)
        {
            try
            {
                _repository.InsertMany(items);
                return items;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual T UpdateItem(T item)
        {
            try
            {
                _repository.Update(item);
                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual T UpdateItem(T item, Type updateModelType)
        {
            var itemUpdated = _repository.GetByID(item.ID);
            if (itemUpdated == null)
            {
                throw new CustomException(ApiResultCode.ItemDoesNotExist);
            }

            var editProperties = updateModelType.GetProperties();
            var updateProperties = typeof(T).GetProperties();

            foreach (var p in editProperties)
            {
                var updateProperty = updateProperties.FirstOrDefault(up => up.Name == p.Name && up.PropertyType == p.PropertyType);
                if (updateProperty != null)
                {
                    var value = updateProperty.GetValue(item);
                    updateProperty.SetValue(itemUpdated, value);
                }
            }

            try
            {
                _repository.Update(itemUpdated);
                return item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual IEnumerable<T> UpdateItems(IEnumerable<T> items)
        {
            try
            {
                _repository.UpdateMany(items);
                return items;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual T DeleteItem(int id)
        {
            var item = _repository.GetByID(id);
            if (item != null)
            {
                try
                {
                    _repository.Delete(item);
                    return item;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                throw new CustomException(ApiResultCode.ItemDoesNotExist);
            }
        }

        public IEnumerable<T> DeleteItems(IEnumerable<int> ids)
        {
            try
            {
                var items = GetItems(ids);

                var idsNotFound = ids.Where(id => !items.Any(i => i.ID == id)).ToArray();
                if (idsNotFound.Any())
                {
                    throw new CustomException(ApiResultCode.ItemDoesNotExist);
                }

                _repository.DeleteMany(items);
                return items;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
