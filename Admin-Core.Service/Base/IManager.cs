﻿using Admin_Core.DataAccess.Base;
using Admin_Core.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Admin_Core.Service.Base
{
    public interface IManager<T> where T : BaseTable
    {
        IEnumerable<T> GetList();
        T GetItem(int id);
        IEnumerable<T> GetItems(IEnumerable<int> ids);
        T AddItem(T item);
        IEnumerable<T> AddItems(IEnumerable<T> items);
        T UpdateItem(T item);
        T UpdateItem(T item, Type updateModelType);
        IEnumerable<T> UpdateItems(IEnumerable<T> items);
        T DeleteItem(int id);
        IEnumerable<T> DeleteItems(IEnumerable<int> ids);
    }
}
