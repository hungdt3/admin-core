﻿using Admin_Core.Common.Attributes;
using Admin_Core.Common.Enums;
using Admin_Core.Common.Exceptions;
using Admin_Core.Common.Helpers;
using Admin_Core.DataAccess.Repositories;
using Admin_Core.Models.DataModels;
using Admin_Core.Service.Base;
using Microsoft.Extensions.DependencyInjection;

namespace Admin_Core.Service.Managers.Administration.Implements
{
    [Service(typeof(IUserManager), ServiceLifetime.Scoped)]
    public class UserManager : Manager<User>, IUserManager
    {
        private readonly IUserRepository _userRepository;

        public UserManager(IUserRepository repository) : base(repository)
        {
            _userRepository = repository;
        }

        public override User AddItem(User item)
        {
            var existedItem = _userRepository.GetByEmail(item.Email);
            if (existedItem != null)
            {
                throw new CustomException(ApiResultCode.ItemAlreadyExist);
            }

            item.Password = EncryptionHelper.EncryptSHA1(item.Password);

            return base.AddItem(item);
        }
    }
}
