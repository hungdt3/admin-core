﻿using Admin_Core.Models.DataModels;
using Admin_Core.Service.Base;

namespace Admin_Core.Service.Managers.Administration
{
    public interface IUserManager : IManager<User>
    {
    }
}
