﻿using Admin_Core.Common.Attributes;
using Admin_Core.Common.Helpers;
using Admin_Core.DataAccess.Base;
using Admin_Core.Service.Base;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin_Core.Configurations
{
    public static class DependencyConfiguration
    {
        public static void Config(IServiceCollection services)
        {
            var dependencyGroups = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(a => a.GetTypes())
                .Where(type => !type.IsGenericType && !type.IsAbstract && !type.IsInterface)
                .Select(type => new
                {
                    Type = type,
                    ServiceAttribute = type.GetCustomAttributes(typeof(ServiceAttribute), true).FirstOrDefault() as ServiceAttribute
                })
                .Where(g => g.ServiceAttribute != null)
                .Select(g => new
                {
                    g.Type,
                    LifeTime = g.ServiceAttribute.GetLifeTime(),
                    InterfaceType = g.ServiceAttribute.GetInterfaceType() ?? g.Type
                });

            foreach (var group in dependencyGroups)
            {
                AddDependency(services, group.Type, group.InterfaceType, group.LifeTime);
            }

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IManager<>), typeof(Manager<>));
        }

        public static void AddDependency(IServiceCollection services, Type type, Type interfaceType, ServiceLifetime lifeTime)
        {
            switch (lifeTime)
            {
                case ServiceLifetime.Singleton:
                    {
                        services.AddSingleton(interfaceType, type);
                        break;
                    }
                case ServiceLifetime.Scoped:
                    {
                        services.AddScoped(interfaceType, type);
                        break;
                    }
                case ServiceLifetime.Transient:
                    {
                        services.AddTransient(interfaceType, type);
                        break;
                    }
            }
        }
    }
}
