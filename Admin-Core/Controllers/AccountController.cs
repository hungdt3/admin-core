﻿using Admin_Core.Common.Enums;
using Admin_Core.Controllers.Base;
using Admin_Core.Models.ApiModels.AccountModels;
using Admin_Core.Service.Services.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;

namespace Admin_Core.Controllers
{
    [Route("api/accounts")]
    [ApiController, Authorize]
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public IActionResult Register(RegisterApiModel model)
        {
            if (model == null || TryValidateModel(model) == false)
            {
                return InvalidParameter();
            }

            return Success(_accountService.Register(model));
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public IActionResult Login(LoginApiModel model)
        {
            if (model == null || TryValidateModel(model) == false)
            {
                return InvalidParameter();
            }

            var profile = _accountService.Login(model.Email, model.Password);
            if (profile == null)
            {
                return Error(ApiResultCode.EmailOrPasswordNotValid);
            }

            return Success(new
            {
                access_token = _accountService.GenerateToken(profile, 8),
                expired_at = DateTime.Now.AddHours(8),
                token_type = "bearer",
                profile
            });
        }

        [HttpGet]
        public IActionResult GetProfile()
        {
            if (int.TryParse(HttpContext.User.FindFirst(ClaimTypes.Sid)?.Value, out int userId))
            {
                return Success(_accountService.GetProfile(userId));
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public IActionResult UpdateProfile(UpdateProfileApiModel model)
        {
            if (model == null || TryValidateModel(model) == false)
            {
                return InvalidParameter();
            }

            if (int.TryParse(HttpContext.User.FindFirst(ClaimTypes.Sid)?.Value, out int userId))
            {
                return Success(_accountService.UpdateProfile(userId, model));
            }
            else
            {
                return BadRequest();
            }
        }
    }
}