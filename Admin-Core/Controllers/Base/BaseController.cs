﻿using Admin_Core.Common.Enums;
using Admin_Core.Models.ApiModels;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Admin_Core.Controllers.Base
{
    public abstract class BaseController : ControllerBase
    {
        [NonAction]
        public OkObjectResult Success() => Ok(new ApiResponseModel()
        {
            StatusCode = Common.Enums.ApiResultCode.Success
        });

        [NonAction]
        public OkObjectResult Success(object data) => Ok(new ApiResponseModel()
        {
            StatusCode = Common.Enums.ApiResultCode.Success,
            Data = data
        });

        [NonAction]
        public OkObjectResult Error(ApiResultCode statusCode) => Ok(new ApiResponseModel()
        {
            StatusCode = statusCode
        });

        [NonAction]
        public OkObjectResult InvalidParameter() => Ok(new ApiResponseModel()
        {
            StatusCode = ApiResultCode.InvalidParameter
        });

        [NonAction]
        public int GetUserID()
        {
            return int.Parse(HttpContext.User.FindFirst(ClaimTypes.Sid).Value);
        }
    }
}
