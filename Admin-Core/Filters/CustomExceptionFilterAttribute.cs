﻿using Admin_Core.Common.Enums;
using Admin_Core.Common.Exceptions;
using Admin_Core.Common.Extensions;
using Admin_Core.Models.ApiModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Admin_Core.Filters
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public CustomExceptionFilterAttribute(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is CustomException)
            {
                var statusCode = (context.Exception as CustomException).StatusCode;

                context.Result = new ObjectResult(new ApiResponseModel()
                {
                    StatusCode = statusCode == 0 ? ApiResultCode.SystemError : statusCode,
                    Message = statusCode == 0 ? context.Exception.Message : statusCode.GetDescription()
                })
                {
                    StatusCode = 200
                };
            }
            else
            {
                if (_hostingEnvironment.IsProduction())
                {
                    context.Result = new ObjectResult("Internal Error") { StatusCode = 500 };
                }
                else
                {
                    context.Result = new ObjectResult(context.Exception) { StatusCode = 500 };
                }
            }
        }
    }
}
