﻿using Admin_Core.Common.Enums;
using Admin_Core.Common.Extensions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Admin_Core.Common.Test.Extensions
{
    public class EnumExtensionTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test_GetDescription()
        {
            var message = ApiResultCode.Success.GetDescription();
            Assert.AreEqual(message, "Success");
        }
    }
}
