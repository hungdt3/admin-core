using Admin_Core.DataAccess.Contexts;
using Admin_Core.DataAccess.Repositories.Implements;
using Admin_Core.Models.DataModels;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NUnit.Framework;
using System;

namespace Admin_Core.DataAccess.Test
{
    public class UserRepositoryTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test_GetByEmail()
        {
            var userHung = new User()
            {
                ID = 1,
                Name = "Hung Dinh The",
                Email = "dinhthehung2010@gmail.com",
                Password = "jLIjfQZ5yojbZGTqxg2pY0VROWQ=",
                Gender = 1,
                MobileNumber = "(+84)-773353139",
                DoB = new DateTime(1989, 11, 19),
                EmailOptIn = "",
                IsDeleted = false
            };

            var options = new DbContextOptionsBuilder<AdminDataContext>()
                                .UseInMemoryDatabase(databaseName: "Test_UserRepository_GetByEmail")
                                .Options;

            using (var context = new AdminDataContext(options))
            {
                context.Set<User>().Add(userHung);
                context.SaveChanges();
            }

            using (var context = new AdminDataContext(options))
            {
                var repository = new UserRepository(context);
                var userActual = repository.GetByEmail("dinhthehung2010@gmail.com");
                Assert.AreEqual(JsonConvert.SerializeObject(userHung), JsonConvert.SerializeObject(userActual));
            }
        }
    }
}