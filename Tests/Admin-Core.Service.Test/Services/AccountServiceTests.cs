using Admin_Core.Common.Exceptions;
using Admin_Core.Common.Helpers;
using Admin_Core.DataAccess.Contexts;
using Admin_Core.DataAccess.Repositories;
using Admin_Core.DataAccess.Repositories.Implements;
using Admin_Core.Models.ApiModels.AccountModels;
using Admin_Core.Models.DataModels;
using Admin_Core.Service.Services.Account;
using Admin_Core.Service.Services.Account.Implements;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace Admin_Core.Service.Test.Services
{
    public class AccountServiceTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        [TestCase("dinhthehung2010@gmail.com", "12345", "{\"ID\":1,\"Name\":\"Hung Dinh The\",\"Email\":\"dinhthehung2010@gmail.com\",\"MobileNumber\":\"(+84)-773353139\",\"Gender\":1,\"DoB\":\"1989-11-19T00:00:00\",\"EmailOptIn\":\"\"}")]
        [TestCase("dinhthehung@gmail.com", "12345", null)]
        [TestCase("dinhthehung2010@gmail.com", "12345678", null)]
        public void Test_Login(string email, string password, string jsonExpected)
        {
            var userHung = new User()
            {
                ID = 1,
                Name = "Hung Dinh The",
                Email = "dinhthehung2010@gmail.com",
                Password = "jLIjfQZ5yojbZGTqxg2pY0VROWQ=",
                Gender = 1,
                MobileNumber = "(+84)-773353139",
                DoB = new DateTime(1989, 11, 19),
                EmailOptIn = "",
                IsDeleted = false
            };

            var options = new DbContextOptionsBuilder<AdminDataContext>()
                              .UseInMemoryDatabase(databaseName: "Test_AccountService_Login")
                              .Options;

            using (var context = new AdminDataContext(options))
            {
                var remainUser = context.Set<User>().FirstOrDefault();
                if (remainUser == null)
                {
                    context.Set<User>().Add(userHung);
                    context.SaveChanges();
                }
            }

            using (var context = new AdminDataContext(options))
            {
                IUserRepository userRepository = new UserRepository(context);
                IOptions<AppSettings> appSettingOptions = Options.Create(new AppSettings() { Secret = "qwieghawugr34#@ty239jkwvbrytg92jdv" });
                IAccountService accService = new AccountService(appSettingOptions, userRepository);

                var profile = accService.Login(email, password);

                if (profile == null)
                {
                    Assert.IsTrue(jsonExpected == null);
                }
                else
                {
                    var actualJson = JsonConvert.SerializeObject(profile);
                    Assert.AreEqual(jsonExpected, actualJson);
                }
            }
        }

        [Test]
        public void Test_GenerateToken()
        {
            var profile = new ProfileApiModel()
            {
                ID = 1,
                Name = "Hung Dinh The",
                Email = "dinhthehung2010@gmail.com",
                Gender = 1,
                MobileNumber = "(+84)-773353139",
                DoB = new DateTime(1989, 11, 19),
                EmailOptIn = ""
            };

            var options = new DbContextOptionsBuilder<AdminDataContext>()
                             .UseInMemoryDatabase(databaseName: "Test_AccountService_GenerateToken")
                             .Options;

            using (var context = new AdminDataContext(options))
            {
                var appSettingOptions = Options.Create(new AppSettings() { Secret = "qwieghawugr34#@ty239jkwvbrytg92jdv" });
                var userRepository = new UserRepository(context);
                var accService = new AccountService(appSettingOptions, userRepository);

                var actualToken = accService.GenerateToken(profile, 8);
                var tokenObject = new JwtSecurityTokenHandler().ReadJwtToken(actualToken);

                Assert.IsFalse(tokenObject == null);

                var id = tokenObject?.Claims?.FirstOrDefault(c => c.Type == ClaimTypes.Sid)?.Value;
                var email = tokenObject?.Claims?.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;

                Assert.AreEqual(profile.ID.ToString(), id);
                Assert.AreEqual(profile.Email, email);
            }
        }

        [Test]
        public void Test_UpdateProfile()
        {
            var userInsert = new User()
            {
                ID = 1,
                Name = "Hung",
                Email = "dinhthehung2010@gmail.com",
                Password = "jLIjfQZ5yojbZGTqxg2pY0VROWQ=",
                Gender = 0,
                MobileNumber = "(+84)-000000000",
                DoB = new DateTime(1988, 1, 1),
                EmailOptIn = "",
                IsDeleted = false
            };

            var profileUpdate = new UpdateProfileApiModel()
            {
                Name = "Hung Dinh The",
                Gender = 1,
                MobileNumber = "(+84)-773353139",
                DoB = new DateTime(1989, 11, 19),
                EmailOptIn = "dinhthehung2010@hotmail.com",
            };

            var userExpected = new ProfileApiModel()
            {
                ID = 1,
                Name = "Hung Dinh The",
                Email = "dinhthehung2010@gmail.com",
                Gender = 1,
                MobileNumber = "(+84)-773353139",
                DoB = new DateTime(1989, 11, 19),
                EmailOptIn = "dinhthehung2010@hotmail.com",
            };

            var options = new DbContextOptionsBuilder<AdminDataContext>()
                             .UseInMemoryDatabase(databaseName: "Test_AccountService_UpdateProfile")
                             .Options;

            using (var context = new AdminDataContext(options))
            {
                context.Set<User>().Add(userInsert);
                context.SaveChanges();
            }

            using (var context = new AdminDataContext(options))
            {
                var appSettingOptions = Options.Create(new AppSettings() { Secret = "qwieghawugr34#@ty239jkwvbrytg92jdv" });
                var userRepository = new UserRepository(context);
                var accService = new AccountService(appSettingOptions, userRepository);

                try
                {
                    var actualUser = accService.UpdateProfile(1, profileUpdate);
                    var actualJson = JsonConvert.SerializeObject(actualUser);
                    var expectedJson = JsonConvert.SerializeObject(userExpected);

                    Assert.AreEqual(expectedJson, actualJson);
                }
                catch (CustomException cex)
                {
                    Assert.IsTrue(false, "Do not expect exception");
                }
            }
        }
    }
}