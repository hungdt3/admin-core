﻿using Admin_Core.DataAccess.Contexts;
using Admin_Core.DataAccess.Extensions;
using Admin_Core.Models.DataModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Admin_Core.DataAccess.Base
{
    public class Repository<T> : IRepository<T>
         where T : BaseTable
    {
        protected AdminDataContext _context;
        private DbSet<T> _entities;

        public virtual IQueryable<T> TableAsNoTracking
        {
            get { return _entities.AsNoTracking().GetActive(); }
        }

        public Repository(AdminDataContext dataContext)
        {
            _context = dataContext;
            _entities = dataContext.Set<T>();
        }

        public virtual IQueryable<T> GetAll()
        {
            return _entities.GetActive();
        }

        public virtual IQueryable<TO> GetTable<TO>() where TO : BaseTable
        {
            return _context.Set<TO>().GetActive();
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        public int Count(Expression<Func<T, bool>> predicate)
        {
            return GetAll().Count(predicate);
        }

        public T GetByID(object id)
        {
            var item = _entities.Find(id);
            if (item == null || item.IsDeleted)
            {
                return null;
            }
            return item;
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return GetAll().Where(predicate).AsEnumerable();
        }

        public void Insert(T item, bool commit = true)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            try
            {
                _entities.Add(item);

                if (commit)
                {
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertMany(IEnumerable<T> items, bool commit = true)
        {
            if (items == null || items.Any(item => item == null))
            {
                throw new ArgumentNullException("items");
            }

            if (!items.Any())
            {
                return;
            }

            try
            {
                _entities.AddRange(items);

                if (commit)
                {
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(T item, bool commit = true)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            try
            {
                _entities.Update(item);

                if (commit)
                {
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateMany(IEnumerable<T> items, bool commit = true)
        {
            if (items == null || items.Any(item => item == null))
            {
                throw new ArgumentNullException("items");
            }

            if (!items.Any())
            {
                return;
            }

            try
            {
                _entities.UpdateRange(items);

                if (commit)
                {
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Not commit
        /// </summary>
        public void UpdateMany(IEnumerable<T> entities, params Expression<Func<T, object>>[] updatedProperties)
        {
            foreach (var item in entities)
            {
                Update(item, updatedProperties);
            }
        }

        /// <summary>
        /// Not commit
        /// </summary>
        public void Update(T item, params Expression<Func<T, object>>[] updatedProperties)
        {
            _entities.Attach(item);
            var entry = _context.Entry(item);
            foreach (var property in updatedProperties)
            {
                entry.Property(property).IsModified = true;
            }
        }

        public void Delete(T item, bool commit = true)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            try
            {
                item.IsDeleted = true;
                if (commit)
                {
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteMany(IEnumerable<T> items, bool commit = true)
        {
            if (items == null || items.Any(item => item == null))
            {
                throw new ArgumentNullException("items");
            }

            if (!items.Any())
            {
                return;
            }

            try
            {
                foreach (var item in items)
                {
                    item.IsDeleted = true;
                }

                if (commit)
                {
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeletePermanent(T item, bool commit = true)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            try
            {
                _entities.Remove(item);
                if (commit)
                {
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}