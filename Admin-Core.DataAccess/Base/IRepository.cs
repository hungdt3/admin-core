﻿using Admin_Core.DataAccess.Base;
using Admin_Core.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Admin_Core.DataAccess.Base
{
    public interface IRepository<T> where T : BaseTable
    {
        IQueryable<T> TableAsNoTracking { get; }

        int Count(Expression<Func<T, bool>> predicate);

        int Commit();

        IQueryable<TO> GetTable<TO>() where TO : BaseTable;
        IQueryable<T> GetAll();

        T GetByID(object id);

        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);

        void Insert(T item, bool commit = true);
        void InsertMany(IEnumerable<T> items, bool commit = true);

        void Update(T item, bool commit = true);
        void UpdateMany(IEnumerable<T> items, bool commit = true);

        void Delete(T entity, bool commit = true);
        void DeleteMany(IEnumerable<T> items, bool commit = true);

        void DeletePermanent(T entity, bool commit = true);

        void UpdateMany(IEnumerable<T> entities, params Expression<Func<T, object>>[] updatedProperties);
        void Update(T item, params Expression<Func<T, object>>[] updatedProperties);
    }
}
