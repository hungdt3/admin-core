﻿using Admin_Core.DataAccess.Base;
using Admin_Core.Models.DataModels;

namespace Admin_Core.DataAccess.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        User GetByEmail(string email);
    }
}
