﻿using Admin_Core.Common.Attributes;
using Admin_Core.DataAccess.Base;
using Admin_Core.DataAccess.Contexts;
using Admin_Core.Models.DataModels;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Admin_Core.DataAccess.Repositories.Implements
{
    [Service(typeof(IUserRepository), ServiceLifetime.Transient)]
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(AdminDataContext dataContext) : base(dataContext)
        {
        }

        public User GetByEmail(string email)
        {
            return GetAll().FirstOrDefault(user => user.Email == email);
        }
    }
}
