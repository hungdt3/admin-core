﻿using Admin_Core.Common.Models;
using Admin_Core.Models.DataModels;
using System.Collections.Generic;
using System.Linq;

namespace Admin_Core.DataAccess.Extensions
{
    public static class IQueryableExtensions
    {
        public static IEnumerable<T> ToPageList<T>(this IQueryable<T> query, PagingData paginationData)
            where T : BaseTable
        {
            if (paginationData != null)
            {
                paginationData.TotalCount = query.Count();
            }
            if (paginationData != null && paginationData.PageIndex > 0 && paginationData.PageSize > 0)
            {
                query = query.OrderByDescending(en => en.ID)
                        .Skip((paginationData.PageIndex - 1) * paginationData.PageSize)
                        .Take(paginationData.PageSize);

            }

            return query.AsEnumerable();
        }

        public static IQueryable<T> GetActive<T>(this IQueryable<T> query) where T : BaseTable
        {
            return query.Where(item => item.IsDeleted == false);
        }
    }
}
