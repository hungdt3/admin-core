﻿using Admin_Core.Models.DataModels;
using Microsoft.EntityFrameworkCore;

namespace Admin_Core.DataAccess.Contexts
{
    public class AdminDataContext : DbContext
    {
        public AdminDataContext(DbContextOptions<AdminDataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<User> Users { get; set; }
    }
}
