﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin_Core.Models.ApiModels
{
    public interface IConvertDataModel<T> where T: class
    {
        T ConvertToDataTable();
    }
}
