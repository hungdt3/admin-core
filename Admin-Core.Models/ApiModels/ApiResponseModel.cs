﻿using Admin_Core.Common.Enums;
using Admin_Core.Common.Extensions;

namespace Admin_Core.Models.ApiModels
{
    public class ApiResponseModel
    {
        public ApiResultCode StatusCode { get; set; }

        private string _message;
        public string Message
        {
            get
            {
                if (!string.IsNullOrEmpty(_message))
                {
                    return _message;
                }
                return StatusCode.GetDescription();
            }
            set
            {
                _message = value;
            }
        }

        public object Data { get; set; }
    }
}
