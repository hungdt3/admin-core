﻿using Admin_Core.Models.DataModels;
using System;

namespace Admin_Core.Models.ApiModels.AccountModels
{
    public class ProfileApiModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public byte Gender { get; set; }
        public DateTime DoB { get; set; }
        public string EmailOptIn { get; set; }

        public ProfileApiModel() { }
        public ProfileApiModel(User user)
        {
            ID = user.ID;
            Name = user.Name;
            Email = user.Email;
            MobileNumber = user.MobileNumber;
            DoB = user.DoB;
            Gender = user.Gender;
            EmailOptIn = user.EmailOptIn;
        }
    }
}
