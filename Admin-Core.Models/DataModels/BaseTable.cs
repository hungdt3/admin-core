﻿using System.ComponentModel.DataAnnotations;

namespace Admin_Core.Models.DataModels
{
    public class BaseTable
    {
        [Key]
        public int ID { get; set; }

        public bool IsDeleted { get; set; }
    }
}
