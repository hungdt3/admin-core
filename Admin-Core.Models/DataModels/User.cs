﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Admin_Core.Models.DataModels
{
    public class User : BaseTable
    {
        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        [Required]
        [StringLength(256)]
        public string Email { get; set; }

        [Required]
        [StringLength(128)]
        public string Password { get; set; }

        [Required]
        [StringLength(32)]
        public string MobileNumber { get; set; }

        [Required]
        public byte Gender { get; set; }

        [Required]
        public DateTime DoB { get; set; }

        [StringLength(256)]
        public string EmailOptIn { get; set; }
    }
}
