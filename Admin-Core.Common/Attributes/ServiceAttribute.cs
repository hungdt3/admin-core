﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Admin_Core.Common.Attributes
{
    public class ServiceAttribute : Attribute
    {
        public Type Type { get; set; }
        public ServiceLifetime LifeTime { get; set; }

        public ServiceAttribute(ServiceLifetime lifeTime = ServiceLifetime.Scoped)
        {
            LifeTime = lifeTime;
        }

        public ServiceAttribute(Type type, ServiceLifetime lifeTime = ServiceLifetime.Scoped)
        {
            Type = type;
            LifeTime = lifeTime;
        }

        public ServiceLifetime GetLifeTime()
        {
            return LifeTime;
        }

        public Type GetInterfaceType()
        {
            return Type;
        }
    }
}
