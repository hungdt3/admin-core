﻿using Admin_Core.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Admin_Core.Common.Exceptions
{
    public class CustomException : Exception
    {
        public ApiResultCode StatusCode { get; set; }

        public CustomException(ApiResultCode resultCode) : base()
        {
            this.StatusCode = resultCode;
        }
    }
}
