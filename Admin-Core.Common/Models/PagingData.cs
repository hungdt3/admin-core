﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Admin_Core.Common.Models
{
    public class PagingData
    {
        public PagingData(int pageIndex, int pageSize)
        {
            this.PageIndex = pageIndex;
            this.PageSize = pageSize;
        }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
    }
}
