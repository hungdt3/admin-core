﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Admin_Core.Common.Enums
{
    public enum ApiResultCode
    {
        [Description("Success")]
        Success = 0,

        [Description("System error")]
        SystemError = 1,

        [Description("Invalid parameter")]
        InvalidParameter = 2,

        [Description("Item does not exist")]
        ItemDoesNotExist = 3,

        [Description("Item already exist")]
        ItemAlreadyExist = 4,

        [Description("Email or password is not valid")]
        EmailOrPasswordNotValid = 5,
    }
}
