﻿using Admin_Core.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Admin_Core.Common.Extensions
{
    public static class EnumExtension
    {
        public static string GetDescription(this ApiResultCode value)
        {
            var memberInfo = typeof(ApiResultCode).GetMember(value.ToString())[0];
            var attributes = memberInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return (attributes[0] as DescriptionAttribute).Description;
            }

            return string.Empty;
        }
    }
}
