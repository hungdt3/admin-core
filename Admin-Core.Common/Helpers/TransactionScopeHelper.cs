﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace Admin_Core.Common.Helpers
{
    public class TransactionScopeHelper
    {
        private readonly static TimeSpan defaultWriteTimeout = TimeSpan.FromSeconds(60);

        public static TransactionScope CreateReadUncommitted()
        {
            return new TransactionScope(
                TransactionScopeOption.Required,
                new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadUncommitted
                });
        }

        public static TransactionScope CreateReadCommitted(TimeSpan? timeout = null)
        {
            return new TransactionScope(
                TransactionScopeOption.Required,
                new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.ReadCommitted,
                    Timeout = timeout ?? defaultWriteTimeout
                });
        }

        public static TransactionScope CreateRepeatableRead()
        {
            return new TransactionScope(
                TransactionScopeOption.Required,
                new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.RepeatableRead
                });
        }

        public static TransactionScope CreateSerializable()
        {
            return new TransactionScope(
                TransactionScopeOption.Required,
                new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.Serializable
                });
        }

        public static TransactionScope CreateSnapshot()
        {
            return new TransactionScope(
                TransactionScopeOption.Required,
                new TransactionOptions
                {
                    IsolationLevel = IsolationLevel.Snapshot
                });
        }
    }
}
