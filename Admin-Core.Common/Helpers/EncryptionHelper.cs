﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Admin_Core.Common.Helpers
{
    public static class EncryptionHelper
    {
        public static string EncryptSHA1(string text)
        {
            var sha = new SHA1Managed();
            var hash = sha.ComputeHash(Encoding.UTF8.GetBytes(text));
            return Convert.ToBase64String(hash);
        }
    }
}
